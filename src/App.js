import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';

import Profile from './components/Profile';
import Vehicle from './components/Vehicle';
import Policy from './components/Policy';
import Quote from './components/Quote';
class App extends Component {
    
  
  render() {
      console.log(this.state);
      return (
              <Router>
                  <div className="App">                      
                      <div className="container-fluid">
                          <Route exact path="/" component={Profile}/>
                          <Route path="/vehicle" component={Vehicle}/>
                          <Route path="/policy" component={Policy}/>     
                          <Route path="/quote" component={Quote}/>                               
                      </div>
                  </div>
              </Router>
              );
  }
};

export default App;