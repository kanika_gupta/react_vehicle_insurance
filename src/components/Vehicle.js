import React, { Component } from 'react';
import { FormControl, FormGroup, FormLabel} from 'react-bootstrap';
import { appendData, fetchData } from '../components/SessionService';
import { getValidator } from '../components/Helper';
import '../App.css';
const brandType = {
    'hero': 'Hero MotoCorp',
    'honda': 'Honda Motor',
    'bajaj': 'Bajaj Auto',
    'tvs' : 'TVS Motors',
    'royal' : 'Royal Enfield',
    'yamaha' : 'Yamaha'
};

const bikeName = {
    'tvsa': 'TVS Apache',
    'reb': 'Royal Enfield Bullet',
    'bp' : 'Bajaj Pulsar',
     'yzr' :'Yamaha YZF',
     'hs' :'Hero Splendor'
};

const variant = {
    'tvsac': 'TVS Apache 120 cc',
    'rebc': 'Royal Enfield Bullet 120cc',
    'bpc' : 'Bajaj Pulsar 120 cc',
     'yzrc' :'Yamaha YZF 120 cc',
     'hsc' :'Hero Splendor cc'
};
const DATA_KEY = "factors";
class Vehicle extends Component {

    constructor(props) {
        super(props);
        this.state = {
            brandType: brandType,
            bikeName: bikeName,
            variant: variant,
            brandTypeValue: null,
            bikeNameValue: null,
            variantValue: null,
            validation: {
                brandTypeValue: ['required'],
                bikeNameValue: ['required'],
                variantValue: ['required']
            }
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    componentWillMount() {
        const bikeData = fetchData(DATA_KEY);

        for (let key of Object.keys(bikeData)) {
            this.setState({[key]: bikeData[key]});
        }

    }

    handleInputChange(e) {
        e.preventDefault();
        const name = e.target.name;
        this.setState({[name]: e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();

        const inputs = {
            brandTypeValue: this.state.brandTypeValue,
            bikeNameValue: this.state.bikeNameValue,
            variantValue: this.state.variantValue
        };

        const validation = getValidator(inputs, this.state.validation);

        if (validation.length > 0) {
            let error = validation.pop();
            alert(error.message);
            return;
        }

        appendData(DATA_KEY, inputs);
        window.location.replace("/policy");
    }

    render() {
        return (
                <div className="App">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="page-header">
                                <h3 className="text-center"> XYZ Motor Insurance Company.</h3>
                            </div>
                        </div>
                        <div className="jumbotron col-sm-8 col-sm-offset-2">
                            <div className="row">
                                <form className="form-horizontal">
                                    <FormGroup
                                        controlId="formBasicText"
                                        >
                                        <FormLabel className="col-sm-4">Brand Name<span className="form-required bold" title="This field is required.">*</span></FormLabel>
                                        <div className="col-sm-8">
                                            <FormControl as="select" custom  placeholder="select"
                                                         onChange={this.handleInputChange}
                                                         name="brandTypeValue"
                                                         defaultValue={this.state.brandTypeValue || ""}
                                                         >
                                                <option value="">Select Brand Type</option>
                                                {Object.keys(this.state.brandType).map(brandKey => <option key={brandKey} value={brandKey}>{this.state.brandType[brandKey]}</option>)}
                                            </FormControl>

                                        </div>
                                    </FormGroup>
                                    <FormGroup
                                        controlId="formBasicText"
                                        >
                                        <FormLabel className="col-sm-4">Bike Name <span className="form-required bold" title="This field is required.">*</span></FormLabel>
                                        <div className="col-sm-8">
                                            <FormControl as="select" custom placeholder="select"
                                                         onChange={this.handleInputChange}
                                                         defaultValue={this.state.bikeNameValue || ""}
                                                         name="bikeNameValue"
                                                         >
                                                <option value="">Select Bike Name</option>
                                                {Object.keys(this.state.bikeName).map(bikeKey => <option key={bikeKey} value={bikeKey}>{this.state.bikeName[bikeKey]}</option>)}
                                            </FormControl>
                                        </div>
                                    </FormGroup>
                                    <FormGroup
                                        controlId="formBasicText"
                                        >
                                        <FormLabel className="col-sm-4">Variant<span className="form-required bold" title="This field is required.">*</span></FormLabel>
                                        <div className="col-sm-8">
                                            <FormControl as="select" custom placeholder="select"
                                                         onChange={this.handleInputChange}
                                                         defaultValue={this.state.variantValue || ""}
                                                         name="variantValue"
                                                         >
                                                <option value="">Select Variant</option>
                                                {Object.keys(this.state.variant).map(variantKey => <option key={variantKey} value={variantKey}>{this.state.variant[variantKey]}</option>)}
                                            </FormControl>
                                        </div>
                                    </FormGroup>
                                    <button type="button" className="btn btn-primary pull-right" onClick={this.handleSubmit.bind(this)} >Continue</button>
                                </form>
                            </div>

                            
                        </div>
                    </div>
                </div>);
    }
}

export default Vehicle;
