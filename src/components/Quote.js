import React, { Component } from 'react';
import { fetchData, getSessionID } from '../components/SessionService';
import '../App.css';

class Quote extends Component {

    render() {
        return (
            <div className="App">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-header">
                            <h3 className="text-center"> XYZ Motor Insurance Company. </h3>                            
                        </div>
                    </div>                    
                </div>
                <div className="row">
                    <div className="jumbotron col-sm-8">
                        <div className="row"> 
                            <div className="panel panel-default">
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-sm-3">                                                           
                                            <h4>Select IDV</h4>
                                        </div>  
                                        <div className="col-sm-3">    
                                            <button type="button" className="btn btn-primary pull-right"  >Rs 30000</button>
                                        </div>
                                        <div className="col-sm-3">                                                   
                                            <button type="button" className="btn btn-primary pull-right"  >Rs 50000</button>
                                        </div>
                                        <div className="col-sm-3">                                                   
                                            <button type="button" className="btn btn-primary pull-right"  >Rs 70000</button>
                                        </div>
                                     </div>
                                </div>    
                            </div>
                        </div> 
                        <div className="row"> 
                            <div className="panel panel-default">
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <div className = "row">
                                                <div className="col-sm-6">
                                                    <h4>No Claim Bonus</h4>
                                                </div>
                                                <div className="col-sm-3">
                                                    <button type="button" className="btn btn-primary pull-right"  >Y</button>
                                                </div>
                                                <div className="col-sm-3">
                                                    <button type="button" className="btn btn-primary pull-right"  >N</button>
                                                </div>
                                            </div>
                                        </div> 
                                        <div className="col-sm-3">
                                            <h4>Last Claim Policy Period</h4>
                                        </div> 
                                        <div className="col-sm-3">
                                            <h4>2020-2021</h4>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>    
                        <div className="row"> 
                            <div className="panel panel-default">
                                <div className="panel-body">
                                    <div className="row">
                                        <div className="col-sm-8">
                                            <div className="row">
                                                <div className="col-sm-6">
                                                    <h4>Do you have PA Cover for 15 lakhs</h4>
                                                </div>
                                                <div className="col-sm-3">
                                                    <button type="button" className="btn btn-primary pull-right"  >Y</button>
                                                </div>
                                                <div className="col-sm-3">
                                                    <button type="button" className="btn btn-primary pull-right"  >Y</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-4">
                                            <h4>PA BOX</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row"> 
                            <h4>Additional Coverage:</h4>
                            <div className="col-sm-8">
                                <div className="row">
                                    <div className="col-sm-2">
                                        <input type = "checkbox"/>
                                    </div>
                                    <div className="col-sm-6">
                                        <h4>Roadside Assistance:</h4>
                                    </div>
                                    <div className="col-sm-4">
                                        <h4>Rs.200</h4>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-2">
                                        <input type = "checkbox"/>
                                    </div>
                                    <div className="col-sm-6">
                                        <h4>PA to unnamed passenger:</h4>
                                    </div>
                                    <div className="col-sm-4">
                                        <h4>Rs.80</h4>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-2">
                                        <input type = "checkbox"/>
                                    </div>
                                    <div className="col-sm-6">
                                        <h4>Legal Liability for paid driver:</h4>
                                    </div>
                                    <div className="col-sm-4">
                                        <h4>Rs.60 for 1 driver</h4>
                                    </div>
                                </div>                                                               
                            </div>
                            <div className="col-sm-4">
                                <div className="row">
                                    <div className="col-sm-8">
                                        <h4>No of Passengers:</h4>
                                    </div>
                                    <div className="col-sm-4">
                                        <h4>1</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="jumbotron col-sm-4"> 
                        <div className="row">  
                            <div className="jumbotron col-sm-4"> 
                            </div>
                            <div className="jumbotron col-sm-8">         
                                <div className="row">
                                    <h4>1 Year:</h4>
                                </div>
                                <div className="row">
                                    <h4>Rs 3941.20:</h4>
                                </div>
                                <div className="row">
                                    <button type="button" className="btn btn-primary"  >Buy Now</button>
                                </div>  
                            </div>                     
                        </div>
                    </div>    
                </div>
            </div>
               );
    }
}

export default Quote;