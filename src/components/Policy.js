import React, { Component } from 'react';
import { appendData, fetchData } from '../components/SessionService';
import { FormControl, FormGroup, FormLabel} from 'react-bootstrap';
import '../App.css';
class Policy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            purchaseYear: "",
            policyExpiryDate: "",
        };

        this.handleInputChange = this.handleInputChange.bind(this);

    }
    
    handleInputChange(e) {
        e.preventDefault();
        const name = e.target.name;

        this.setState({[name]: e.target.value});
    }

    componentWillMount() {
        const policyData = fetchData('policy');

        for (let key of Object.keys(policyData)) {
            this.setState({[key]: policyData[key]});
        }
    }
    handleSubmit(e) {
        let inputs = {
            purchaseYear: this.state.purchaseYear,
            policyExpiryDate: this.state.policyExpiryDate,
        };

        appendData('policy', inputs);
        window.location.replace("/quote");
    }
render() {
    return (
            <div className="App">

                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-header">
                            <h3 className="text-center"> XYZ Motor Insurance Company. </h3>
                        </div>
                    </div>
                    <div className="jumbotron col-sm-8 col-sm-offset-2">
                        <form className="form-horizontal">
                        <FormGroup
                                controlId="formBasicText"
                                >
                                <FormLabel className="col-sm-4">Month and Year of Purchase<span className="form-required bold" title="This field is required.">*</span></FormLabel>
                                <div className="col-sm-8">
                                    <FormControl
                                        type="date"
                                        value={this.state.purchaseYear}
                                        onChange={this.handleInputChange}
                                        name="purchaseYear"
                                        />
                                </div>
                            </FormGroup>    
                            <FormGroup
                                controlId="formBasicText"
                                >
                                <FormLabel className="col-sm-4">Previous Policy Expiry Date<span className="form-required bold" title="This field is required.">*</span></FormLabel>
                                <div className="col-sm-8">
                                    <FormControl
                                        type="date"
                                        value={this.state.policyExpiryDate}
                                        onChange={this.handleInputChange}
                                        name="policyExpiryDate"
                                        />
                                </div>
                            </FormGroup>                                 
                            <button type="button" className="btn btn-primary pull-right" onClick={this.handleSubmit.bind(this)} >View Price</button>
                        </form>
                    </div>
                </div>
            </div>);
}
}

export default Policy;
