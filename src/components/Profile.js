import React, { Component } from 'react';
import { appendData, fetchData } from '../components/SessionService';
import { getValidator } from '../components/Helper';
import { FormControl, FormGroup, FormLabel} from 'react-bootstrap';
import '../App.css';
class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            regNumber: "",
            email: "",
            mobile: "",
            validation: {
                regNumber: ['required'],
                email: ['required', 'email'],
                mobile: ['required']
            }
        };

        this.handleInputChange = this.handleInputChange.bind(this);

    }
    
    handleInputChange(e) {
        e.preventDefault();
        const name = e.target.name;

        this.setState({[name]: e.target.value});
    }

    componentWillMount() {
        const profileData = fetchData('profile');

        for (let key of Object.keys(profileData)) {
            this.setState({[key]: profileData[key]});
        }
    }
    handleSubmit(e) {
        let inputs = {
            regNumber: this.state.regNumber,
            email: this.state.email,
            mobile: this.state.mobile
        };

        const validation = getValidator(inputs, this.state.validation);

        if (validation.length > 0) {
            let error = validation.pop();
            alert(error.message);
            return;
        }

        appendData('profile', inputs);
        window.location.replace("/vehicle");
    }
render() {
    return (
            <div className="App">

                <div className="row">
                    <div className="col-sm-12">
                        <div className="page-header">
                            <h3 className="text-center"> XYZ Motor Insurance Company. </h3>
                        </div>
                    </div>
                    <div className="jumbotron col-sm-8 col-sm-offset-2">
                        <form className="form-horizontal">
                        <FormGroup
                                controlId="formBasicText"
                                >
                                <FormLabel className="col-sm-4">Registration No. <span className="form-required bold" title="This field is required.">*</span></FormLabel>
                                <div className="col-sm-8">
                                    <FormControl
                                        type="text"
                                        value={this.state.regNumber}
                                        placeholder="M2221G33543"
                                        onChange={this.handleInputChange}
                                        name="regNumber"
                                        />
                                </div>
                            </FormGroup>      
                            <FormGroup
                                controlId="formBasicText"
                                >
                                <FormLabel className="col-sm-4">Mobile Number <span className="form-required bold" title="This field is required.">*</span></FormLabel>
                                <div className="col-sm-8">
                                    <FormControl
                                        type="text"
                                        value={this.state.mobile}
                                        placeholder="9837126434"
                                        onChange={this.handleInputChange}
                                        name="mobile"
                                        />
                                </div>
                            </FormGroup>                                             


                            <FormGroup
                                controlId="formBasicText"
                                >
                                <FormLabel className="col-sm-4">E-Mail Address <span className="form-required bold" title="This field is required.">*</span></FormLabel>
                                <div className="col-sm-8">
                                    <FormControl
                                        type="email"
                                        value={this.state.email}
                                        placeholder="John@gmail.com"
                                        onChange={this.handleInputChange}
                                        name="email"
                                        />
                                </div>
                            </FormGroup>
 
                            <button type="button" className="btn btn-primary pull-right" onClick={this.handleSubmit.bind(this)} >Get Quote</button>
                        </form>
                    </div>
                </div>
            </div>);
}
}

export default Profile;
